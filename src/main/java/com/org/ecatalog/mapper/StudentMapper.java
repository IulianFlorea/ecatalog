package com.org.ecatalog.mapper;

import com.org.ecatalog.dto.SchoolClassDto;
import com.org.ecatalog.dto.StudentCreateDto;
import com.org.ecatalog.dto.StudentDto;
import com.org.ecatalog.entity.Student;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class StudentMapper {

    private final SchoolClassMapper schoolClassMapper;

    public Student toEntity(StudentCreateDto createDto) {
        return Student.builder()
                .firstName(createDto.getFirstName())
                .lastName(createDto.getLastName())
                .cnp(createDto.getCnp())
                .schoolClassId(createDto.getSchoolClassId())
                .build();

    }


    // (StudentDto <- SchoolClassDto)    <=  school class mapper  <=   (Student <- SchoolClass)
    public StudentDto toDto(Student entity) {

        SchoolClassDto schoolClassDto = schoolClassMapper.toDto(entity.getSchoolClass());

        return StudentDto.builder()
                .id(entity.getId())
                .cnp(entity.getCnp())
                .firstName(entity.getFirstName())
                .lastName(entity.getLastName())
                .schoolClass(schoolClassDto)
                .build();

    }
}
